`qt-installer-framework` for some reason fails to build with Docker ...

See [run1.sh](run1.sh) for an example on how to build the [Dockerfile1](Dockerfile1).

My output of `run1.sh` is saved in [run1.sh.log](run1.sh.log)

---

Problem solved by [adjusting pacman configuration](https://aur.archlinux.org/pkgbase/qt-installer-framework/#comment-836177) of the base image (archlinux:base-devel).
